/**
 * Implements an optimization algorithm based on pairwise rankings of
 * vectors in the form of a proposal method.
 */
import { get_random_parameter_vector } from "./_simulation/controller";
import type { Ranking } from "./_data";
import type { DataPoint } from "./_data";
import { get_new_vector_id } from "./_data";
import { get_vector } from "./_data";


/**
 * Returns true if and only if there is no element `p2` in `points` such that
 * `p1` < `p2` according to the order in `rankings`.
 */
function is_greatest(
		p1: string,
		points: string[],
		rankings: Ranking[],
		): boolean {
	for(const link of rankings)
		if(link.lo_id == p1 && points.includes(link.hi_id))
			return false;
	return true;
}


/**
 * Return a mutated version of the parameter vector.
 */
function mutated(
		vector: number[],
		scale: number,
		): number[] {
	let m = [];
	for(const x of vector)
		m.push(x + (Math.random()*2-1)*scale);
	return m;
}


/*
 * Comparison proposal heuristic: compare two mutated versions of
 * a greatest element. It is assumed that the list of rankings is not empty.
 */
async function proposal_heuristic_mutated_greatest(
		rankings: Ranking[],
		state_size: number,
		action_size: number,
		): Promise<[DataPoint, DataPoint]> {
	// Get all unique points in the rankings
	let points = [];
	for(const ranking of rankings)
	for(const point of [ranking.hi_id, ranking.lo_id])
	if(!(points.includes(point)))
		points.push(point);

	// Get a random greatest element
	points.sort((a, b) => {Math.random()});
	let greatest_id = points[0];
	for(const point of points)
	if(is_greatest(point, points, rankings)) {
		greatest_id = point;
		break;
	}
	let greatest_vector = await get_vector(greatest_id);

	let mutation_rate = 0.1*Math.random();
	let mutated_vector = mutated(greatest_vector, mutation_rate);
	let p1 = {
		id: greatest_id,
		vector: greatest_vector,
	};
	let p2 = {
		id: get_new_vector_id(),
		vector: mutated_vector,
	};
	return [p1, p2];
}


/**
 * Return a pair of parameter vectors that are optimal for ranking. This
 * function receives the state size and the action size of the
 * robot because they are used to compute the size of the parameter vectors.
 *
 * TODO: this function should provide some information-theoretic guarantees
 * and implement a sensible solution to the exploration-exploitation dilemma.
 */
async function get_ranking_proposal(
		rankings: Ranking[],
		state_size: number,
		action_size: number,
		): Promise<[DataPoint, DataPoint]> {
	// If there are no rankings, return two random vectors.
	if(rankings.length == 0) {
		let zero = [];
		let init_scale = 0.1;
		for(const _ of get_random_parameter_vector(state_size, action_size))
			zero.push(0.0);
		return [
			{
				id: get_new_vector_id(),
				vector: mutated(zero, init_scale),
			},
			{
				id: get_new_vector_id(),
				vector: mutated(zero, init_scale),
			}
		];
	}

	// Choose a proposal heuristic
	let heuristics = [
		proposal_heuristic_mutated_greatest,
	];
	let heuristic= heuristics[Math.floor(Math.random()*heuristics.length)];
	let proposal = await heuristic(
		rankings,
		state_size,
		action_size,
	);

	// Randomly sort the two points in the proposal
	proposal.sort((a, b) => {Math.random()});
	return [proposal[0], proposal[1]];
}

export {
	get_ranking_proposal,
};
