/**
 * Definition of controllers as Multi Layer Perceptrons (MLP).
 */
type Controller = (state: number[]) => number[];

const MLPParameters = {
	hidden_sizes: [16, 16,],
}

/**
 * Return a random list of numbers parametrizing a linear controller:
 *  f(x) = W@x + b
 */
function get_random_parameter_vector_linear(
		ins: number,
		outs: number,
	): number[] {

	// It's easy to compute the number of parameters in a linear controller
	let parameter_vector_size = ins*outs + outs;

	// Generate a random parameter vector
	let vector = [];
	let scale = 1.0/Math.sqrt(ins);
	for(let i = 0; i < parameter_vector_size; i++)
		vector.push((Math.random()*2-1)*scale);
	return vector;
}

/**
 * Return a random list of numbers parametrizing an MLP.
 */
function get_random_parameter_vector_mlp(
		ins: number,
		outs: number,
		hidden_sizes: number[],
		): number[] {
	let last_outs = ins;
	let vector = [];
	for(const size of hidden_sizes) {
		let layer_vector = get_random_parameter_vector_linear(
			last_outs,
			size,
		);
		vector.push(...layer_vector);
		last_outs = size;
	}
	let layer_vector = get_random_parameter_vector_linear(
		last_outs,
		outs,
	);
	vector.push(...layer_vector);
	return vector;
}

/**
 * Return a random list of numbers parametrizing a controller from this module.
 */
function get_random_parameter_vector(
		state_size: number,
		action_size: number,
	): number[] {
	return get_random_parameter_vector_mlp(
		state_size,
		action_size,
		MLPParameters.hidden_sizes,
	);
}

/**
 * Return the dot product of the two vectors.
 */
function vecdot(v1: number[], v2: number[]): number {
	let v3 = 0.0;
	for(let i = 0; i < v1.length; i++)
		v3 += v1[i]*v2[i];
	return v3;
}


/**
 * Return the value of W@v where W is a matrix and v is a vector.
 */
function matvec_mul(mat: number[][], vec: number[]): number[] {
	let v = [];
	for(let i = 0; i < mat.length; i++)
		v.push(vecdot(mat[i], vec));
	return v;
}

/**
 * Return a pair-wise addition of the lists.
 */
function vec_add(v1: number[], v2: number[]): number[] {
	let v3 = [];
	for(let i = 0; i < v1.length; i++)
		v3.push(v1[i]+v2[i]);
	return v3;
}


/**
 * Use the given parameter vector to compute an action W@x + b.
 */
function get_action_linear(
		parameter_vector: number[],
		input: number[],
		outs: number,
	): number[] {
	// Parse parameter vector into a matrix and a vector
	let matrix = [];
	let i = 0;
	for(let j = 0; j < outs; j++) {
		let row = [];
		for(let j2 = 0; j2 < input.length; j2++) {
			row.push(parameter_vector[i]);
			i++;
		}
		matrix.push(row);
	}
	let b = [];
	for(let j = 0; j < outs; j++) {
		b.push(parameter_vector[i]);
		i++;
	}

	// Use controller
	let action = vec_add(matvec_mul(matrix, input), b);
	return action;
}


/**
 * Use the given parameter vector to compute an action. The returned
 * list has all values in [-1, 1]. This interprets the parameter vector as
 * the parameters of an MLP.
 */
function get_action(
		parameter_vector: number[],
		state: number[],
		action_size: number,
	): number[] {
	// Iterate through the hidden layers
	let last_outs = state.length;
	let i = 0;
	let v = state;
	for(const size of MLPParameters.hidden_sizes) {
		// Extract the parameter vector of the current layer
		let ins = last_outs;
		let outs = size;
		let layer_vector_size = ins*outs + outs;
		let layer_vector = parameter_vector.slice(i, i+layer_vector_size);
		i += layer_vector_size;
		last_outs = outs;

		// Update current vector with tanh(W@v + b)
		v = get_action_linear(
			layer_vector,
			v,
			outs
		).map(Math.tanh);
	}
	
	// Compute the final layer
	let final_layer_vector = parameter_vector.slice(i, parameter_vector.length);
	v = get_action_linear(
		final_layer_vector,
		v,
		action_size,
	);
	return v.map(Math.tanh);
}

export type {
	Controller
};

export {
	get_random_parameter_vector,
	get_action
};
