/**
 * Simulation definition using Matter.js for a soft robot.
 */

import Matter from 'matter-js';
import type { Controller } from "./controller";


const particleOptions = {
		friction: 0.7,
		frictionStatic: 0.9,
		render: { visible: true }
	};

const constraint_options = {};

/**
 * Add some sine waves to the state
 */
const sine_periods = [
	200,
	400,
	600,
	800,
];


/**
 * Return a Matter.Composite defining the robot.
 */
function create_robot() {
	// module aliases
	let Composites = Matter.Composites,
		Bodies = Matter.Bodies,
		Common = Matter.Common;

	// Define the robot
	let robot_x = 100;
	let robot_y = 100;
	let columns = 2;
	let rows = 2;
	let columnGap = 10;
	let rowGap = 30;
	let crossBrace = true;
	let particleRadius = 25;

	let particleOptions_ = Common.extend(
		{ inertia: Infinity },
		particleOptions
	);

	let constraintOptions_ = Common.extend(
		{ stiffness: 0.03, render: { type: 'line', anchors: false } },
		constraint_options
	);

	let circles = Composites.stack(
			robot_x,
			robot_y,
			columns,
			rows,
			columnGap,
			rowGap,
			function(x: number, y: number) {
				return Bodies.circle(x, y, particleRadius, particleOptions_);
			}
		);
	let robot = Composites.mesh(
			circles,
			columns,
			rows,
			crossBrace,
			constraintOptions_
		);

	return robot;
}


/**
 * Return a new Matter.Engine adding a ground object, the given robot,
 * and a callback to the simulation step function that calls the given
 * controller to compute actions that are applied on the robot.
 *
 * The controller is expected to be a function mapping a vector with entries
 * in [-1, 1] to equally sized vectors with entries in [-1, 1]. The numbers
 * in these vectors represent the normalized current length and new target
 * length of each corresponding spring in the robot.
 */
function create_engine(
		robot: Matter.Composite,
		controller: Controller,
		): Matter.Engine {
	// module aliases
	let Engine = Matter.Engine,
		Composite = Matter.Composite,
		Bodies = Matter.Bodies,
		Events = Matter.Events;

	// Add the ground and the robot objects
	let ground = Bodies.rectangle(400, 610, 810, 60, { isStatic: true });

	// Create an engine
	let engine = Engine.create();
	Composite.add(engine.world, [
			ground,
			robot,
		]);

	// Find the original length of the springs
	let original_spring_lengths = [];
	let springs = Composite.allConstraints(robot);
	for(const spring of springs)
		original_spring_lengths.push(spring.length);

	// Track contact between circles and the ground
	let is_circle_ground_contact = [];
	let circles = Composite.allBodies(robot);
	for(const _ of circles)
		is_circle_ground_contact.push(false);
	Events.on(engine, "collisionStart", (event: Matter.Event) => {
		// For each contact pair, check if it is a ground-circle pair
		for(const pair of event.pairs)
		for(let i = 0; i < circles.length; i++)
		if(pair.id == Matter.Pair.id(circles[i], ground)) // id(a, b) == id (b, a)
			is_circle_ground_contact[i] = true;
	});
	Events.on(engine, "collisionEnd", (event: Matter.Event) => {
		// For each contact pair, check if it is a ground-circle pair
		for(const pair of event.pairs)
		for(let i = 0; i < circles.length; i++)
		if(pair.id == Matter.Pair.id(circles[i], ground)) // id(a, b) == id (b, a)
			is_circle_ground_contact[i] = false;
	});

	// Set the callback to inject control inputs
	let control_frequency_in_ms = 100;
	let last_control_timestamp = 0;
	let max_spring_expansion = 3;
	Events.on(engine, "beforeUpdate", function(event: Matter.Event) {
		// Do not do anything if not enough time has passed
		if(last_control_timestamp === 0)
			last_control_timestamp = event.timestamp;
		if(event.timestamp - last_control_timestamp < control_frequency_in_ms)
			return;
		last_control_timestamp = event.timestamp;

		// Compute the state of the robot by linearly normalizing the current
		// spring lengths by the original spring lengths and linearly
		// interpolating to [-1, 1].
		let state = [];
		for(let i = 0; i < springs.length; i++) {
			let current_length = Matter.Vector.magnitude(Matter.Vector.sub(
				Matter.Constraint.pointAWorld(springs[i]),
				Matter.Constraint.pointBWorld(springs[i]),
			));
			let normalized_length = current_length/(max_spring_expansion*original_spring_lengths[i]);
			state.push(2*normalized_length-1);
		}

		// Add a time variable
		for(const sine_period of sine_periods)
			state.push(Math.sin(event.timestamp/sine_period));

		// Add contact information
		for(const is_contact of is_circle_ground_contact)
			state.push(is_contact ? 1.0 : -1.0);

		// Use the controller to compute an action
		let action = controller(state);

		// Change the target lengths of the springs by linearly interpolating the
		// action to [0, 2], so that an input of zero tries to contract the
		// spring completely and an input of 1 tries to expand the spring to
		// twice its original length.
		for(let i = 0; i < springs.length; i++)
			springs[i].length = ((action[i]+1.0)/2)*original_spring_lengths[i]*max_spring_expansion;
		});

	return engine;
}

/**
 * Return the size of the state vectors.
 */
function get_state_size(robot: Matter.Composite): number {
	// State is composed of:
	// - the current length of each spring
	// - time variables: sin(t_in_ms/period_i)
	// - contact for each circle-ground pair
	return Matter.Composite.allConstraints(robot).length
		+ sine_periods.length
		+ Matter.Composite.allBodies(robot).length;
}

/**
 * Return the size of the action vectors.
 */
function get_action_size(robot: Matter.Composite): number {
	return Matter.Composite.allConstraints(robot).length;
}


export {
	create_robot,
	create_engine,
	get_state_size,
	get_action_size,
};
