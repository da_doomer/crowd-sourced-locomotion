/**
 * Data types and Firebase backend semantics.
 */

import { initializeApp } from "@firebase/app";
import { getDatabase } from '@firebase/database';
import { get } from '@firebase/database';
import { ref } from '@firebase/database';
import { push } from '@firebase/database';
import { child } from '@firebase/database';
import { update } from '@firebase/database';

const firebaseConfig = {
  apiKey: "AIzaSyCXSZASLA79sUj09r245Q_D77whJUBuANA",
  authDomain: "crowd-sourced-locomotion.firebaseapp.com",
  projectId: "crowd-sourced-locomotion",
  storageBucket: "crowd-sourced-locomotion.appspot.com",
  messagingSenderId: "815286584193",
  appId: "1:815286584193:web:af444f567886640732de80"
};
const app = initializeApp(firebaseConfig);
const database = getDatabase();

/**
 * A DataPoint associates vectors with IDs.
 */
type DataPoint  = {
	vector: number[],
	id: string,
};

/**
 * A Ranking is a statement of the form "hi is strictly better than lo", where
 * hi and lo are DataPoints.
 */
type Ranking = {
	hi_id: string,
	lo_id: string,
};

function get_new_vector_id(): string {
	return push(child(ref(database), "vectors")).key;
}

/**
 * Return the Rankings in the database.
 */
async function get_rankings(): Promise<Ranking[]> {
	let rankings_data = (await get(ref(database, "rankings"))).val();
	if(rankings_data === null)
		return [];
	let rankings = [];
	for(const ranking_id of Object.keys(rankings_data)) {
		let hi_id = rankings_data[ranking_id].hi_id;
		let lo_id = rankings_data[ranking_id].lo_id;
		let ranking = {
			hi_id: hi_id,
			lo_id: lo_id,
		};
		rankings.push(ranking);
	}
	return rankings;
};

/**
 * Return the vector from the database.
 */
async function get_vector(id: string): Promise<number[]> {
	let vector = (await get(ref(database, "vectors/"+id))).val();
	if(vector === null)
		throw "Could not fetch vector from database!";
	return vector;
};

/**
 * Create a Ranking "hi is better than lo" and add it to the database.
 */
async function add_ranking(lo: DataPoint, hi: DataPoint) {
	// The rankings database only contains the hi and lo IDs
	let ranking_id = push(child(ref(database), "rankings")).key;
	let updates = {};
	updates["/rankings/" + ranking_id] = {lo_id: lo.id, hi_id: hi.id};

	// The vectors database contains the actual vectors
	updates["/vectors/" + lo.id] = lo.vector;
	updates["/vectors/" + hi.id] = hi.vector;
	await update(ref(database), updates);
}

export type {
	DataPoint,
	Ranking,
};

export {
	get_rankings,
	add_ranking,
	get_new_vector_id,
	get_vector,
};
