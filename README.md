# Crowd sourced-locomotion

Can the crowd teach locomotion from scratch?

See the [page live](https://da_doomer.gitlab.io/crowd-sourced-locomotion).

## Development

Run `npm run install` and then `npm run dev` to start a local server.
